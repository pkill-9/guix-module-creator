#!/usr/bin/env python3
#https://www.gnu.org/software/guix/manual/guix.html#Invoking-guix-import
#https://docs.python.org/3/library/argparse.html#argparse.Action
#Error when using package path: no code for module (#{}# home itsme guix-recipes dev polybar)

import json, argparse, re, subprocess, tempfile, os, logging

CACHEFILE = os.path.expanduser("~/.cache/guix-module-creator-file-hashes.log")

def add_hash_from_file(metadata: dict, cachefile: str):
    """Read from cache file which stores url hashes as '<url> <hash>'."""

    with open(cachefile, "r") as f:

        cached_hashes = f.read()
        if len(cached_hashes) > 0:
            cached_hashes = cached_hashes.strip('\n')
            cached_hashes = cached_hashes.split('\n')
            cached_hashes = [line.split(' ') for line in cached_hashes]
            logging.debug("Cached hashes array: {}".format(cached_hashes))
            cached_hashes = {source : source_hash for source, source_hash in cached_hashes}
        else:
            cached_hashes = []

    if metadata["source"] in cached_hashes:
        logging.info("Using cached hash")
        source_hash = cached_hashes[metadata["source"]]
        source = {
            "method": "url-fetch",
            "uri": metadata["source"],
            "sha256": {
              "base32": source_hash
            }
          }
        metadata["source"] = source
        return metadata
    elif metadata["source"] not in cached_hashes:
        logging.info("Hash not found in cache")
        subprocess_call = subprocess.run(["guix", "download", metadata["source"]], stdout=subprocess.PIPE)

        if subprocess_call.returncode != 0:
            raise Exception("`Guix download` failed with code {}".format(subprocess_call.returncode))
        else:
            source_hash = subprocess_call.stdout.decode().split('\n')[1]
            source = {
                "method": "url-fetch",
                "uri": metadata["source"],
                "sha256": {
                  "base32": source_hash
                }
              }
            metadata["source"] = source
            with open(CACHEFILE, "a") as f:
                f.write("{} {}\n".format(metadata["source"]["uri"], source_hash))
            return metadata


def manufacture_pkgdef(metadata: dict, with_boilerplate=True):
    def generate_arguments_text(metadata: dict):
        BOILERPLATE = "(arguments `({}))"
        arguments_texts = []
#        if arguments.get("make-flags"):
        def list_to_thing(thing_type, flags):
            #add "#:make-flags '("<flag1>" "<flag2")" to arguments_texts
            flags = ['"' + flag + '"' for flag in flags] # list: '"flag1"', '"flag2"'
            flags = " ".join(flags) # string: '"flag1" "flag2"'
            thing_with_flags = "#:{} '({})".format(thing_type, flags)

            return thing_with_flags
        if metadata.get('make-flags'):
            arguments_texts.append(list_to_thing("make-flags", metadata["make-flags"]))
        if metadata.get('configure-flags'):
            arguments_texts.append(list_to_thing("configure-flags", metadata["configure-flags"]))
        #if configure-flags
          #add "..." to arguments_texts
        return BOILERPLATE.format("\n".join(arguments_texts))

    with tempfile.NamedTemporaryFile(mode="w", delete=False) as f:
        tempfile_name = f.name
        json.dump(metadata, f)

    logging.debug("Tmpfile name: {}".format(f.name))
    logging.debug("Tmpfile contents: {}".format(metadata))
    subprocess_call = subprocess.run(["guix", "import", "json", f.name], stdout=subprocess.PIPE)
    os.remove(tempfile_name)

    if subprocess_call.returncode != 0:
        raise Exception("`Guix import` failed with code {}".format(subprocess_call.returncode))
    else:
        generated_package = subprocess_call.stdout.decode()

    #if arguments in metadata
      #arguments_text = generate_arguments_text(metadata)
      #add arguments_text after "(build-system *)" in generated_package
    if metadata.get("make-flags") or metadata.get("configure-flags"):
        arguments_text=generate_arguments_text(metadata)
        generated_package += "  " + arguments_text
    
    if with_boilerplate:
        return HEADER + generated_package + "\n )\n(package (inherit {}))".format(metadata["name"])
    else:
        return generated_package

parser = argparse.ArgumentParser(description='Generate a Guix module.')
parser.add_argument('--source', type=str, required=True,
        help="(Required) URL of package source")
parser.add_argument('--build-system', type=str, required=True, metavar="BUILD-SYSTEM", choices=["gnu", "ant", "asdf", "cargo", "cmake", "go", "glib-or-gtk", "minify", "ocaml", "python", "perl", "r", "texlive", "ruby", "waf", "scons", "haskell", "dub", "emacs", "font", "meson", "trivial"],
                            help="(Required) gnu/python/etc")
parser.add_argument('--native-inputs', metavar='INPUT', type=str, nargs='*',
                            help='Additional build inputs')
parser.add_argument('--inputs', metavar='INPUT', type=str, nargs='*',
                            help='Additional build and runtime inputs')
parser.add_argument('--propagated-inputs', metavar='INPUT', type=str, nargs='*',
                            help='Additional runtime inputs')
#parser.add_argument('--make-flags', metavar='MAKE-FLAGS', type=str, nargs='*',
#                            help='Make flags')
parser.add_argument('--configure-flags', metavar='CONFIGURE-FLAGS', type=str, nargs='*',
                            help='Configure flags')
parser.add_argument('--name', metavar='PACKAGE-NAME', type=str,
                            help='Will attempt to parse from URL if not specified')
parser.add_argument('--version', metavar='PACKAGE-VERSION', type=str,
                            help='Will attempt to parse from URL if not specified')
parser.add_argument('--home-page', metavar='URL', type=str, default="")
parser.add_argument('--synopsis', metavar='SYNOPSIS', type=str, default="")
parser.add_argument('--description', metavar='DESCRIPTION', type=str, default="")
parser.add_argument('--license', metavar='LICENSE', type=str, choices=["GPL-3.0+"], default="GPL-3.0+")
parser.add_argument('--no-download', action="store_true")
parser.add_argument('--no-boilerplate', action="store_true")
parser.add_argument('--debug', action="store_true")
args = parser.parse_args()
metadata = vars(args)

for key in metadata:
    metadata[key.replace('_', '-')] = metadata.pop(key) # change underscores to dashes

match = re.match(r"^.+\/([\w-]+)-([\d\.]+)\.[\w\.]+", metadata["source"])

if not metadata["name"]:
    if match:
        metadata["name"] = match.group(1)
    else:
        raise Exception("Couldn't extract name and/or version.")

if not metadata["version"]:
    if match:
        metadata["version"] = match.group(2)
    else:
        raise Exception("Couldn't extract version and/or version.")

HEADER = """(define-module ({})
  #:use-module (gnu packages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system {})
  #:use-module (guix licenses)
 )

; Example arguments to use:
;    (arguments
;     `(#:tests? #f
;       #:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
;       #:phases (modify-phases %standard-phases (delete 'configure))
;       ))

(define-public {}
""".format(metadata["name"], metadata["build-system"], metadata["name"])

if args.debug:
    logging.basicConfig(level=logging.DEBUG, format="{message}", style="{")
else:
    logging.basicConfig(level=logging.INFO, format="{message}", style="{")

print(args)
if args.no_download:
    HEADER += "\n;Real url: {}\n\n".format(args.source)
    dummy_url= "mirror://gnu/hello/hello-2.10.tar.gz"
    metadata["source"] = dummy_url

if args.no_boilerplate:
    WITH_BOILERPLATE = False
else:
    WITH_BOILERPLATE = True

print(manufacture_pkgdef(add_hash_from_file(metadata, CACHEFILE), with_boilerplate=WITH_BOILERPLATE))
#If additional make-flags and such, process them in manufacture_pkgdef(): find (build-system *) with regex, and add a string beginning with \n that itself adds the flags to the arguments. `guix import json` ignores arguments so it's safe to add them to the "metadata" object.
